import requests, json

body = {
    "message": "hello world!"
}

body = json.dumps(body)

headers = {'Content-Type': 'application/json'}

#r = requests.get("http://localhost/")

r = requests.post("http://localhost/upper", data=body, headers=headers)
#r = requests.post("http://localhost/calcular", data=body, headers=headers, auth=("usuario", "senha"))

j = r.json()
print(j['message'])
#print(r.text)